import * as React from "react";
import * as ReactDOM from "react-dom";

import {App} from "./components/app/App";

const rootElement = document.getElementById("root");
ReactDOM.render(
    <App/>,
    rootElement
);
