import * as React from "react";
import "./Btn.scss"

interface BtnProps {
    width?: number,
    height?: number,
    text: string,
    backgroundColor?: string,
    textColor?: string
    onClick?: any
}

export const Btn = React.memo(({width, height, text, backgroundColor, textColor, onClick}: BtnProps) => {
    return (
        <a onClick={onClick} className="btn" href="#" style={{width: `${width}px`, height: `${height}px`, backgroundColor: backgroundColor, color: textColor}}>{text}</a>
    )
})