import * as React from "react";
import {useCallback, useState} from "react";
import "./App.scss"
import {OurStory} from "../ourStory/OurStory";
import {MovieStore} from "../movieStore/MovieStore"
import {Footer} from "../footer/Footer";
import {Header} from "../header/Header";
import {MovieDetails} from "../movieDetails/MovieDetails"


export const App = () => {
    const [movieDetailsOpened, setMovieDetailsOpened] = useState(false)
    const [movieDetailsTitle, setMovieDetailsTitle] = useState('')
    const [movieDetailsGenre, setMovieDetailsGenre] = useState([])
    const [movieDetailsYear, setMovieDetailsYear] = useState(0)
    const [movieDetailsImage, setMovieDetailsImage] = useState('')
    const [movieDetailsDescription, setMovieDetailsDescription] = useState('')


    const onClickMovie = (title: string, genre: string[], year: number, image: string, description: string) => {
        setMovieDetailsOpened(true)
        setMovieDetailsTitle(title)
        setMovieDetailsGenre(genre)
        setMovieDetailsYear(year)
        setMovieDetailsImage(image)
        setMovieDetailsDescription(description)
    }

    const onClickSearchMovie = () => {
        setMovieDetailsOpened(false)
    }

    return (
        <>
            {!movieDetailsOpened ?
                <OurStory/> :
                <MovieDetails title={movieDetailsTitle} year={movieDetailsYear} genre={movieDetailsGenre} image={movieDetailsImage} description={movieDetailsDescription} />
            }
            <Header searchIcon={movieDetailsOpened} onClickSearchMovie={onClickSearchMovie} />

            <MovieStore onClickMovie={onClickMovie} />
            <Footer />
        </>
    )
}