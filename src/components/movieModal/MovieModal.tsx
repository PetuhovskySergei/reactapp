import * as React from "react";
import "./MovieModal.scss";
import {Btn} from "../btn/Btn";

interface MovieModalProps {
    title: string,
    isDeleteModal?: boolean,
    onClose: any,
    titleItem?: string,
    genre?: string[],
    year?: number,
    image?: string
}

export const MovieModal = ({title, isDeleteModal, onClose, titleItem, genre, year, image}: MovieModalProps) => {
    return (
        <>
            <div className="overflow"></div>
            <div className="movieModal">
                <div className="closeBtn" onClick={onClose}>&#10006;</div>
                <div className="movieModal__header">{title}</div>
                <div className="movieModal__body">
                    {!isDeleteModal &&
                        <div className="movieOptions">
                            {/*<div className="movieOptions__item">*/}
                            {/*    <label htmlFor="movieId">MOVIE ID</label>*/}
                            {/*    <span id="movieId">test</span>*/}
                            {/*</div>*/}
                            <div className="movieOptions__item">
                                <label htmlFor="movieTitle">TITLE</label>
                                <input id="movieTitle" type="text" placeholder={titleItem ? titleItem :"Type title"}/>
                            </div>
                            <div className="movieOptions__item">
                                <label htmlFor="movieDate">RELEASE DATE</label>
                                <input id="movieDate" type="text" placeholder={year ? year.toString() : "Select Date"}/>
                            </div>
                            <div className="movieOptions__item">
                                <label htmlFor="movieUrl">MOVIE URL</label>
                                <input id="movieUrl" type="text" placeholder="Movie URL here"/>
                            </div>
                            <div className="movieOptions__item">
                                <label htmlFor="movieGenre">GENRE</label>
                                <input id="movieGenre" type="text" placeholder={genre ? genre.join(", ") : "Select Genre"}/>
                            </div>
                            <div className="movieOptions__item">
                                <label htmlFor="movieOverview">OVERVIEW</label>
                                <input id="movieOverview" type="text" placeholder="Overview here"/>
                            </div>
                            <div className="movieOptions__item">
                                <label htmlFor="movieRuntime">RUNTIME</label>
                                <input id="movieRuntime" type="text" placeholder="Runtime here"/>
                            </div>
                        </div>
                    }
                    {isDeleteModal &&
                        <div>Are you sure you want to delete this movie?</div>
                    }
                </div>
                <div className="movieModal__footer">
                    {isDeleteModal &&
                        <Btn
                            width={155}
                            height={55}
                            text={"CONFIRM"}
                            onClick={onClose}
                        />
                    }
                    {!isDeleteModal &&
                        <>
                            <Btn
                                width={155}
                                height={55}
                                text={"RESET"}
                                backgroundColor={"rgba(10, 10, 10,0.7)"}
                                textColor={"#F65261"}
                                onClick={onClose}
                            />
                            <Btn
                                width={155}
                                height={55}
                                text={"SUBMIT"}
                                onClick={onClose}
                            />
                        </>
                    }

                </div>
            </div>
        </>
    )
}
