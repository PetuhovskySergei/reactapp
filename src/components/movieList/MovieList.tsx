import * as React from "react";
import {useState, useCallback, useMemo} from "react";
import "./MovieList.scss";
import {MovieListItem} from "../movieListItem/MovieListItem"
import {MovieModal} from "../movieModal/MovieModal";

interface MovieListProps {
    movieList: any[],
    onClickMovie: any
}

export const MovieList = ({movieList, onClickMovie}: MovieListProps) => {
    const [editModalOpened, setEditModalOpened] = useState(false);
    const [deleteModalOpened, setDeleteModalOpened] = useState(false);


    const onClickEdit = useCallback(() => {
        setEditModalOpened(!editModalOpened)
    }, [editModalOpened])

    const onClickDelete = useCallback(() => {
        setDeleteModalOpened(!deleteModalOpened)
    }, [deleteModalOpened])

    return (
        <div className="movieList">
            <div className="moviesFound">
                <span>{movieList.length}</span> movies found
            </div>
            <div className="movieListItems">
                    {useMemo(() =>
                        movieList.map((e) => {
                            return <>
                                <MovieListItem key={e.id} title={e.title} genre={e.genre} year={e.year} image={e.image} description={e.description} onClickDelete={onClickDelete} onClickEdit={onClickEdit} onClickMovie={onClickMovie} />
                                {editModalOpened &&
                                <MovieModal title={"EDIT MOVIE"} onClose={onClickEdit} titleItem={e.title} genre={e.genre} year={e.year} />
                                }
                                {deleteModalOpened &&
                                <MovieModal title={"DELETE MOVIE"} onClose={onClickDelete} isDeleteModal={true} />
                                }
                            </>
                        }), movieList
                    )}
            </div>
        </div>
    )
}
