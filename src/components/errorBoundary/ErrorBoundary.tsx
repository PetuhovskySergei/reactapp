import * as React from "react";

interface Error {
    stack?: string;
}

export class ErrorBoundary extends React.Component {

    public state = {
        redirect: "",
        hasError: false
    };

    static getDerivedStateFromError(error: any) {
        return { hasError: true };
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        console.log(error, errorInfo)
    }

    render() {
        if (this.state.hasError) {
            return <h1>Something went wrong.</h1>;
        }

        return this.props.children;
    }
}