import * as React from "react";
import "./MovieListItem.scss";
import {useState, useCallback} from "react";

interface MovieListItemProps {
    title: string,
    genre: string[],
    year: number,
    image: string,
    description: string,
    onClickEdit: any,
    onClickDelete: any,
    onClickMovie: any
}

export const MovieListItem = ({title, genre, year, image, onClickEdit, onClickDelete, onClickMovie, description}: MovieListItemProps) => {
    const [optionsOpened, setOptionsOpened] = useState(false);
    let genreList = genre.join(', ')

    const onClickOptions = (e: any) => {
        setOptionsOpened(!optionsOpened)
        e.stopPropagation()
    }

    return (
        <>
            <div className="movieListItems__item" onClick={useCallback(() => onClickMovie(title, genre, year, image, description), onClickMovie)}>
                <div className="movieListItems__item__options" onClick={onClickOptions}>&#65049;</div>
                {optionsOpened &&
                    <div className="movieListItems__item__optionsDropdown">
                        <span onClick={onClickOptions}>&#10006;</span>
                        <div onClick={onClickEdit}>Edit</div>
                        <div onClick={onClickDelete}>Delete</div>
                    </div>
                }
                <div style={{backgroundImage: `url(${image})`}} className="movieListItems__item__img" />
                <div className="movieListItems__item__title">
                    <span>{title}</span>
                    <div>{year}</div>
                </div>
                <div className="movieListItems__item__genre">{genreList}</div>
            </div>

        </>
    )
}
