import * as React from "react";
import {Header} from "../header/Header";
import {Btn} from "../btn/Btn";
import "./OurStory.scss";

export const OurStory = () => {
    return (
        <>
            <div className="ourStoryContent">
                <div className="ourStoryContent__searchBlock">
                    <h1>FIND YOUR MOVIE</h1>
                    <div className="ourStoryContent__searchBlock__search">
                        <input className="searchMovie" placeholder={"What do you want to watch?"} />
                        <Btn
                            width={155}
                            height={55}
                            text={"SEARCH"}
                        />
                    </div>
                </div>

            </div>
            <div className="ourStory"></div>
        </>
    )
}
