import * as React from "react";
import "./MovieFilter.scss";

export const MovieFilter = () => {
    return (
        <div className="movieFilter">
            <ul className="genreList">
                <li className="genreList__item">ALL</li>
                <li className="genreList__item">DOCUMENTARY</li>
                <li className="genreList__item">COMEDY</li>
                <li className="genreList__item">HORROR</li>
                <li className="genreList__item">CRIME</li>
            </ul>
            <div className="sortBy">
                <span>SORT BY</span>
                <div>RELEASE DATE</div>
            </div>
        </div>
    )
}
