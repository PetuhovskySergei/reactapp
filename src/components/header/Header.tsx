import * as React from "react";
import {useState, useCallback} from "react";
import './header.scss';
import {Btn} from "../btn/Btn";
import {MovieModal} from "../movieModal/MovieModal";

interface HeaderProps {
    searchIcon: boolean,
    onClickSearchMovie: any
}

export const Header = ({searchIcon, onClickSearchMovie}: HeaderProps) => {
    const [addMovieOpen, setAddMovieOpen] = useState(false)
    const addMovie = "ADD MOVIE";
    const onClickAddMovie = useCallback(() => {
        setAddMovieOpen(!addMovieOpen)
    }, [addMovieOpen])

    return (
        <>
            <header className="header">
                <div className="logo">Logo</div>
                {!searchIcon ?
                    <Btn
                        width={155}
                        height={55}
                        text={`+${addMovie}`}
                        backgroundColor={"rgba(10, 10, 10,0.7)"}
                        textColor={"#F65261"}
                        onClick={onClickAddMovie}
                    /> :
                    <div className="search" onClick={onClickSearchMovie}>&#128270;</div>
                }

            </header>
            {addMovieOpen &&
                <MovieModal title={addMovie} onClose={onClickAddMovie} />
            }
        </>
    )
}