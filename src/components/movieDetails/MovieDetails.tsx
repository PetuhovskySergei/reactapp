import * as React from "react";
import "./MovieDetails.scss"
import {useEffect, useMemo} from "react";

interface MovieDetailsProps {
    title: string,
    genre: string[],
    year: number,
    image: string,
    description: string
}

export const MovieDetails = React.memo(({title, genre, year, image, description}: MovieDetailsProps) => {
    let genreList = useMemo(() => genre.join(', '), [])

    useEffect(() => {
        window.scrollTo(0, 0);
    })

    return (
        <div className="movieDetails">
            <div className="movieDetails__movieImg"></div>
            <div className="movieDetails__movieDescription">
                <div className="movieDetails__movieDescription__title">
                    <h3>{title}</h3>
                    <div className="movieDetails__movieDescription__mark"></div>
                </div>
                <div className="movieDetails__movieDescription__genre">{genreList}</div>
                <div className="movieDetails__movieDescription__year">
                    <span>{year}</span><span>154 min</span>
                </div>
                <div className="movieDetails__movieDescription__text">
                    {description}
                </div>
            </div>
        </div>
    )
})